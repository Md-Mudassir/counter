const points = document.querySelectorAll(".counter");
const speed = 500; // The lower the slower

let address = location.href;

let pages = [
  "https://pointscounter.netlify.com/",
  "https://pointscounter.netlify.com/pages/page2.html",
  "https://pointscounter.netlify.com/pages/page3.html",
  "https://pointscounter.netlify.com/pages/page4.html",
];

//Random number generator within a range
const random = (min, max) => Math.floor(Math.random() * (max - min) + min);

points.forEach((counter) => {
  const setCount = (value) =>
    counter.setAttribute("data-target", value + random(100, 200)); //Range 100 - 200

  const updateCount = () => {
    pages[1] == address
      ? setCount(100)
      : pages[2] == address
      ? setCount(200)
      : pages[3] == address
      ? setCount(300)
      : setCount(0);

    const target = +counter.getAttribute("data-target");
    const count = +counter.innerText;

    // Lower inc to slow and higher to slow
    const inc = target / speed;

    // Check if target is reached
    if (count < target) {
      // Add inc to count and output in counter
      counter.innerText = Math.ceil(count + inc);
      // Call function every ms
      setTimeout(updateCount, 1);
    } else {
      counter.innerText = target;
    }
  };
  updateCount();
});
